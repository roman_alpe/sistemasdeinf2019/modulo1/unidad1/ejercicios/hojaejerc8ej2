﻿DROP DATABASE IF EXISTS b20191706;

CREATE DATABASE b20191706;
-- Seleccionar la base de datos
USE b20191706;
/*
  creando la tabla 
*/

CREATE TABLE Zona_Urbana(
  Nombre_Zona varchar(50),
  Categoria varchar(15),
  PRIMARY KEY(Nombre_Zona) -- creando la clave
);

CREATE TABLE bloque_casas(
  Numero int,
  calle varchar(100),
  Npisos int,
  Nombre_Zona varchar(50),
  PRIMARY KEY(Numero,calle),
  CONSTRAINT FKbloque_casas FOREIGN KEY(Nombre_Zona)
  REFERENCES Zona_Urbana(Nombre_Zona)
  );

CREATE TABLE Casa_Particular(
  Numero int,
  Nombre_Zona varchar(50),
  m2 int,
  PRIMARY KEY(Numero,Nombre_Zona), -- creando la clave
  CONSTRAINT FKCasPart_Zona FOREIGN KEY(Nombre_Zona)
  REFERENCES Zona_Urbana(Nombre_Zona)

);

CREATE TABLE persona(
  dni varchar(20),
  Nombre varchar(50),
  edad int,
  PRIMARY KEY(dni)
  );

CREATE TABLE Vive_C(
  Numero int,
  Nombre_Zona varchar(50),
  dni varchar(20),
  m2 int,
  Nombre varchar(50),
  edad int,
  PRIMARY KEY(Numero,Nombre_Zona,dni), -- creando la clave
  CONSTRAINT FKVive_Casa FOREIGN KEY(Numero,Nombre_Zona)
  REFERENCES Casa_Particular(Numero,Nombre_Zona),
  CONSTRAINT FKVive_Persona FOREIGN KEY(dni)
  REFERENCES Persona(dni),
  UNIQUE(dni)
  );

CREATE TABLE Piso(
  planta int,
  puerta varchar(10),
  Numero int,
  calle varchar(50),
  m2 int,
  PRIMARY KEY(planta,puerta,Numero,calle)
  );

CREATE TABLE Vive_P(
  dni varchar(20),
  planta int,
  puerta varchar(10),
  Numero int,
  calle varchar(50),
  PRIMARY KEY(dni,planta,puerta,Numero,calle),
  UNIQUE(dni),
  CONSTRAINT FKVive_Persona1 FOREIGN KEY(dni) 
  REFERENCES Persona(dni),
  CONSTRAINT FKVive_Piso FOREIGN KEY (Planta,Puerta,Numero,calle) 
  REFERENCES Piso(planta,puerta,Numero,calle)
  
  
  );



CREATE TABLE Posee_C(
  dni varchar(20),
  Numero int,
  Nombre_Zona varchar(50),
  PRIMARY KEY(dni,Numero,Nombre_Zona),
  CONSTRAINT FKPosee_Persona FOREIGN KEY(dni)
  REFERENCES persona(dni),
  CONSTRAINT FKPosee_Cas_Part FOREIGN KEY(Numero,Nombre_Zona)
  REFERENCES Casa_Particular(Numero,Nombre_Zona)
 
  );

CREATE TABLE Posee_P(
  dni varchar(20),
  Planta int,
  Puerta varchar(10),
  calle varchar(50),
  numero int,
  PRIMARY KEY(dni,Planta,Puerta,Calle,Numero),
  CONSTRAINT FKPosee_Persona1 FOREIGN KEY(dni)
  REFERENCES persona(dni),
  CONSTRAINT FKPosee_Piso FOREIGN KEY(Planta,Puerta,numero,calle)
  REFERENCES Piso(Planta,Puerta,numero,calle)
  );

